# VUE-CLI

[![Home](https://www.enfi.com.cn/images/2022081817183146554.png)](https://www.enfi.com.cn)

完整的 [node.js](https://nodejs.org/zh-cn/) 解决方案。

- [ENFI-CLI](#ENFI-CLI)
  - [基础](#基础)
    - [路由](#路由)
    - [页面](#页面)
    - [权限](#权限)
    - [平台模式](#平台模式)
  - [工具类](#工具类)
  - [支持](#支持)

## 基础
---

### 路由

#### 创建一个路由

> 在文件路径 `/src/router/index.js` 下的 `constantRoutes` 内编辑

> 基础路由配置遵循 `vue-router` 规则

#### 路由结构

> 带有 “?” 的为非必传参数

```json
// example
{
  // 路径
  path: '/example',
  // 别名
  name: 'example',
  // 组件
  component: () => import('@/views/example'),
  meta?: {
    // 标题
    title?: '示例列表',
    // 权限值
    permission?: 'example:list',
    // 是否隐藏（菜单配置）
    hidden?: true,
    // 图标，来源/src/icons/svg和antd内图标
    icon?: 'permission'
  }
}

// OR

{
  path: '/example/detail',
  name: 'exampleDetail',
  component: () => import('@/views/example/Detail.vue'),
  meta?: {
    title?: '示例详情',
    permission?: 'example:detail',
    hidden?: true,
    icon?: 'permission'
  }
}
```

### 页面

#### 创建一个页面

> 在文件路径 `/src/views` 目录下创建对应页面

```sh
# 创建expmple列表页
1.在/src/views下新建文件夹example
2.新建文件index.vue

# 创建example详情页
1.在/src/views/example下新建文件Detail.vue
```

### 权限

#### 路由权限

> 通过配置路由参数 `meta:{ permission:'xxxx' }` 控制；

```sh
登录后获取用户信息接口，拿到接口返回数据中的 `permissions:['aaa','bbb',...]` 参数，遍历permissions匹配到相应路由的permission路由访问权限放开，否则不能访问（具体实现逻辑示例在`store/modules/permission.js`文件中）
```

#### 按钮权限

> 方式一：在相应按钮组件中使用 `v-auth="xxx"`指令，当用户信息的permissions参数中包含v-auth配置的参数，按钮权限放开。否则不显示该按钮

> 方式二：在相应按钮组件中使用`v-if="hasPermission('xxx')"`，匹配逻辑与方式一相同

> ps： 具体实现示例在`views/permission/AuthPageC.vue`

#### 其他配置

> 1、用户登录、退出、获取用户信息、角色、token校验等功能实现在`store/modules/user.js`文件中

> 2、路由拦截逻辑实现在`src/permission.js`文件中

> 3、...后续完善....

### 平台模式

> 提供 `sso` 、 `business` 、 `default` 三种模式

```sh
# sso
统一认证平台，可作门户使用

# business
常规业务系统使用

# default
单机模式，常用于内部项目开发
```

> 在文件路径 `/src/helper/config.js` 下的 `systemType` 配置

```sh
# 配置平台模式
systemType: 'default'

# 统一认证平台地址---可配置为默认统一认证平台地址--按需配置
ssoUrl: 'http://localhost:8080'

# 默认重定向地址---可配置为默认跳转的业务系统地址--按需配置
redirectUrl: document.querySelector('html').dataset.promiseSsoUrl || 'http://localhost:8081',

# 默认子系统id---在恩菲后台管理系统配置--按需配置
clientId: document.querySelector('html').dataset.promiseClientId || 'enfi-cli-local',
clientSecret: document.querySelector('html').dataset.promiseClientSecret || '123456'
```

## 工具类
---

### 

## 支持
---

当前版本的 CLI 在 LTS 版本的 Node 上完全支持。Node 版本应不低于v12。
建议使用最新的长期支持版本。