const path = require('path')
const webpack = require('webpack')
process.env.VUE_APP_VERSION = require('./package.json').version
process.env.VUE_APP_T = require('./package.json').t
process.env.VUE_APP_S = require('./package.json').s
function resolve (dir) {
  return path.join(__dirname, dir)
}
module.exports = {
  publicPath: process.env.BASE_URL,
  outputDir: process.env.VUE_APP_OUTPUT_DIR,
  lintOnSave: false,
  productionSourceMap: false,
  devServer: {
    disableHostCheck: true,
    proxy: {
      '/api': {
        target: process.env.VUE_APP_API_URL,
        ws: true,
        changeOrigin: true,
        pathRewrite: { '^/api': '' }
      }
    },
    port: 8080
  },
  css: {
    loaderOptions: {
      stylus: {
        import: path.resolve(__dirname, 'src/static/styl/*.styl')
      },
      less: {
        modifyVars: {
          'border-radius-base': '2px'
        },
        javascriptEnabled: true
      }
    }
  },
  configureWebpack: {
    plugins: [
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'windows.jQuery': 'jquery'
      })
    ]
  },
  transpileDependencies: [
    // 可以是字符串或正则表达式
    'ant-design-vue'
  ],
  chainWebpack (config) {
    // set svg-sprite-loader
    config.module.rule('svg').exclude.add(resolve('src/assets/icons')).end()
    config.module
      .rule('icons')
      .test(/\.svg$/)
      .include.add(resolve('src/assets/icons'))
      .end()
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
      .options({
        symbolId: 'icon-[name]'
      })
      .end()
  }
}
