// /* vue混入 antd icon 多语言 */
// export const langMixin = {
//   data () {
//     return {
//       lang: this.$store.state.Lang.language
//     }
//   },
//   mounted () {
//     this.onMixin()
//   },
//   methods: {
//     onMixin () {
//       if (this.$store.state.Lang.language === 'zh') {
//         this.$i18n.locale = this.$store.state.Lang.language
//       } else {
//         this.$i18n.locale = this.$store.state.Lang.language
//       }
//     }
//   }

// }
/**
 * vue 混入
 * 主要功能是获取实时时间
 **/
export default {
  data () {
    return {

    }
  },
  components: {
  },
  mounted () {
    // this.onMixin()
  },
  methods: {
    /**
       * 引入语言包
       * @param path
       */
    importFontpack (path) {
      if (this.$store.state.Lang.language === 'zh') {
        this.$i18n.locale = this.$store.state.Lang.language
      } else {
        this.$i18n.locale = this.$store.state.Lang.language
      }
      const _nowMsg = require('@/common' + path + this.$store.state.Lang.language + '.json')
      this.$i18n.mergeLocaleMessage(this.$store.state.Lang.language, _nowMsg)
    }
  }
}
