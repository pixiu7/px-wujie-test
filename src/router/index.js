import Vue from 'vue'
import VueRouter from 'vue-router'
import admin from './admin'
import demo from './demo'
import error from './error'
// import permission from './permission'
import sso from './sso'

Vue.use(VueRouter)
// 权限路由
export const asyncRoutes = [
  // permission
]
// 基础路由
export const constantRoutes = [
  ...admin,
  ...error,
  ...sso,
  ...demo,
  {
    path: '/preview',
    name: 'preview',
    component: () => import('@/views/preview')
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/login/Index'),
    meta: { hidden: true }
  },
  {
    path: '/Register',
    name: 'Register',
    component: () => import('@/views/registerlogin/Register.vue'),
    meta: { hidden: true }
  }
]
// 捕获调用push切换到同一路由时报错的异常。
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (location) {
  return originalPush.call(this, location).catch(err => err)
}
// 捕获调用replace切换到同一路由时报错的异常。
const originalReplace = VueRouter.prototype.replace
VueRouter.prototype.replace = function replace (location) {
  return originalReplace.call(this, location).catch(err => err)
}

const createRouter = () => new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [...constantRoutes]
})

const router = createRouter()
// 重置路由
export function resetRouter () {
  const newRouter = createRouter();
  (router).matcher = (newRouter).matcher // reset router
}
export default router
