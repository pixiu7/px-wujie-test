const ssoLAYOUT = () => import('@/components/ssoLayout')
export default [
  {
    path: '/sso',
    name: 'sso',
    redirect: '/sso/index',
    component: ssoLAYOUT,
    meta: { hidden: true },
    children: [
      {
        path: 'index',
        name: 'ssoIndex',
        component: () => import('@/views/sso')
      }
    ]
  }
]
