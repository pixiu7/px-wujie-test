// 定义模拟的权限路由

// 角色类型
export const LAYOUT = () => import('@/components/layout')
const permission = {
  path: '/permission',
  name: 'permission',
  component: LAYOUT,
  redirect: '/permission/AuthPageA',
  meta: {
    title: '权限菜单',
    icon: 'permission'
  },
  children: [
    {
      path: 'auth-pageA',
      name: 'AuthPageA',
      component: () => import('@/views/permission/AuthPageA.vue'),
      meta: {
        permission: 'permission:AuthPageA',
        title: '权限测试页A'
      }
    },
    {
      path: 'auth-pageB',
      name: 'AuthPageB',
      component: () => import('@/views/permission/AuthPageB.vue'),
      meta: {
        permission: 'permission:AuthPageB',
        title: '权限测试页B'
      }
    },
    {
      path: 'auth-pageC',
      name: 'AuthPageC',
      component: () => import('@/views/permission/AuthPageC.vue'),
      meta: {
        permission: 'permission:AuthPageC',
        title: '权限测试页C'
      }
    }
  ]
}

export default permission
