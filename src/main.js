// import '@babel/polyfill'
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Ant from 'ant-design-vue'
import './assets/css/default.less'
import dink from 'dinkjs'
import Viewer from 'v-viewer'
import WujieVue from 'wujie-vue2'
import 'viewerjs/dist/viewer.css'
import './helper/core'
import config from './helper/config'
import http from './helper/http'
import helper from './helper/helper'
import tab from './helper/tab'
import * as echarts from 'echarts'
import i18n from '@/common/i18n'
import { setupGlobDirectives } from './directives'
import './permission'
import '@/assets/icons'
// 增加混入
import shareUtils from '@/mixin/utils'
Vue.mixin(shareUtils)
Vue.prototype.$bus = new Vue()
Vue.config.productionTip = false
Vue.prototype.$echarts = echarts
Vue.prototype.$tab = tab

const { bus, setupApp, preloadApp, destroyApp } = WujieVue
Vue.use(WujieVue)
const lifecycles = {
  beforeLoad: (appWindow) => console.log(`${appWindow.__WUJIE.id} beforeLoad 生命周期`),
  beforeMount: (appWindow) => console.log(`${appWindow.__WUJIE.id} beforeMount 生命周期`),
  afterMount: (appWindow) => console.log(`${appWindow.__WUJIE.id} afterMount 生命周期`),
  beforeUnmount: (appWindow) => console.log(`${appWindow.__WUJIE.id} beforeUnmount 生命周期`),
  afterUnmount: (appWindow) => console.log(`${appWindow.__WUJIE.id} afterUnmount 生命周期`),
  activated: (appWindow) => console.log(`${appWindow.__WUJIE.id} activated 生命周期`),
  deactivated: (appWindow) => console.log(`${appWindow.__WUJIE.id} deactivated 生命周期`),
  loadError: (url, e) => console.log(`${url} 加载失败`, e)
}
setupApp({
  name: 'previewApp', url: '//localhost:8888', exec: true, sync: true, alive: true,
  // ...lifecycles
})
preloadApp({ name: 'previewApp' })

Vue.use(Ant)
Vue.use(dink)
Vue.use(Viewer)
Vue.prototype.$config = config
Vue.prototype.$http = http
Vue.prototype.$helper = helper
setupGlobDirectives(Vue)
new Vue({
  router,
  store,
  i18n,
  render: h => h(App),
  data: {
    Bus: new Vue()
  }
}).$mount('#app')
