export const setItem = (key, value) => {
  if (typeof value === 'object') {
    value = JSON.stringify(value)
  }
  window.localStorage.setItem(key, value)
}
export const getItem = key => {
  const value = window.localStorage.getItem(key)
  try {
    return JSON.parse(value)
  } catch (err) {
    return value
  }
}
// SHANCHUBENDISHUJU
export const deleteItem = key => {
  window.localStorage.removeItem(key)
}
// HUOQUBENCISHIJIAN
export const getTime = () => {
  const date = new Date().getTime() + (5000)
  setItem('time', date)
  return date
}
