import { message, notification } from 'ant-design-vue/es'
import store from 'store'
import configLocal from './config'
const crypto = require('crypto')
const moment = require('moment')
message.config({ top: '1rem' })

export default {
  // dateFormat
  now: () => {
    return Date.parse(new Date())
  },
  utd (value, format = 'YYYY-MM-DD') {
    return moment(parseInt(value)).format(format)
  },
  dtu (value) {
    return moment(value).valueOf()
  },
  charge (value) {
    return typeof value === 'number' ? value : this.decrypt(value)
  },
  // decrypt
  decrypt (data) {
    try {
      const cipher = crypto.createDecipheriv('aes-256-cbc', configLocal.encrypt.key, configLocal.encrypt.iv)
      let result = cipher.update(data, 'hex', 'utf8')
      result += cipher.final('utf8')
      return JSON.parse(result)
    } catch (e) {
      return ''
    }
  },
  // encrypt
  encrypt (data) {
    try {
      const cipher = crypto.createCipheriv('aes-256-cbc', configLocal.encrypt.key, configLocal.encrypt.iv)
      let result = cipher.update(JSON.stringify(data), 'utf8', 'hex')
      result += cipher.final('hex')
      return result
    } catch (e) {
      return ''
    }
  },
  // msg
  msg: {
    success (msg) {
      message.success(msg)
    },
    warn (msg) {
      message.warn(msg)
    },
    error (msg) {
      message.error(msg)
    }
  },
  // notification
  notification: {
    success: (msg) => {
      notification.success({ message: '提示', description: msg })
    },
    warn: (msg) => {
      notification.warn({ message: '提示', description: msg })
    },
    error: (msg) => {
      notification.error({ message: '提示', description: msg })
    }
  },
  pageHide () {
    this.storage.set('portal', JSON.stringify(store.state))
  },
  // storage
  storage: {
    get (key) {
      if (key) {
        return store.get(key)
      } else {
        const tmp = {}
        store.each((v, k) => {
          tmp[k] = v
        })
        return tmp
      }
    },
    set (key, value) {
      return store.set(key, value)
    },
    remove (key) {
      return store.remove(key)
    },
    clear () {
      return store.clearAll()
    }
  },
  timeFix () {
    const time = new Date()
    const hour = time.getHours()
    return hour < 9 ? '早上好' : hour <= 11 ? '上午好' : hour <= 13 ? '中午好' : hour < 20 ? '下午好' : '晚上好'
  },
  tod (value) {
    return value / (1000 * 86400)
  },
  welcome () {
    // const arr = ['休息一会儿吧', '准备吃什么呢?', '要不要打一把 DOTA', '我猜你可能累了']
    const arr = ['祝你开心每一天！']
    const index = Math.floor(Math.random() * arr.length)
    return arr[index]
  },
  // 配置token项
  getToken () {
    return this.storage.get('token')
  },
  setToken (token) {
    this.storage.set('token', token)
  },
  removeToken () {
    this.storage.remove('token')
  }
}
