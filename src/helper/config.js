export default {
  // 启用encrypt
  encrypt: {
    enable: false,
    key: 'abcdefghigklmnopqrstuvwxyz123456',
    iv: 'abcdefghigklmnop',
    salt: 'dinkstudio'
  },
  // 启用mock
  mock: true,
  // mock: false,
  // 启用ws
  ws: true,
  // ws: false,
  // 平台类型  sso 统一认证平台 || business 业务系统 || default 内部系统开发使用-登录不分离
  systemType: 'default',
  // 统一认证平台地址---可配置为默认统一认证平台地址--按需配置
  ssoUrl: process.env.VUE_APP_SSO_URL,
  // 默认重定向地址---可配置为默认跳转的业务系统地址--按需配置
  redirectUrl: document.querySelector('html').dataset.promiseSsoUrl || process.env.VUE_APP_REDIRECT_URL,
  // 默认子系统id---在恩菲后台管理系统配置--按需配置
  clientId: document.querySelector('html').dataset.promiseClientId || process.env.VUE_APP_CLIENT_ID,
  clientSecret: document.querySelector('html').dataset.promiseClientSecret || process.env.VUE_APP_CLIENT_SECRET
}
